package com.atlassian.bitbucket.coverage.converter

import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.COVERED_KEY
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.PARTLY_COVERED_KEY
import com.atlassian.bitbucket.coverage.converter.AbstractCoverageConverter.Companion.UNCOVERED_KEY
import com.atlassian.bitbucket.coverage.jacoco.Counter
import org.assertj.core.api.Assertions.assertThat
import org.hamcrest.Matchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.`when`
import java.io.File
import java.lang.Thread.currentThread
import java.nio.file.Paths

const val ANY_STRING = "anystring"

class JacocoConverterTest {

    private val jacocoConverter = JacocoConverter()

    private fun String.toFile(): File = File(
            currentThread()
                    .contextClassLoader
                    .getResource(this)!!
                    .file)

    @Test
    fun readJacocoXmlReport() {
        val reportFile = "jacoco-report.xml".toFile()
        val entity = jacocoConverter.convert(reportFile, Paths.get(""))
        assertThat(entity.files).hasSize(11)
    }

    @Test
    fun readJacocoXmlReportParallel() {
        val reportFile = "jacoco.xml".toFile()
        val entity = jacocoConverter.convertParallel(reportFile, Paths.get(""), 2)
        assertThat(entity.files).hasSize(191)
    }

    @Test
    fun foo() {
        val reportFile = "jacoco-aggregate.xml".toFile()
        val entity = jacocoConverter.convert(reportFile, Paths.get(""))
        assertThat(entity.files).hasSize(25)
        assertThat(entity.files.map { it.path })
                .contains("com/atlassian/billing/gateway/feign/BillingApiGatewayFeignClientAutoConfiguration")
                .hasSize(25)
    }

    @Test
    fun getBlockCoverageStateCovered() {
        assertThat(jacocoConverter.getBlockCoverageState("0", "1"), `is`(COVERED_KEY))
    }

    @Test
    fun getBlockCoverageStateUncovered() {
        assertThat(jacocoConverter.getBlockCoverageState("1", "0"), `is`(UNCOVERED_KEY))
    }

    @Test
    fun getBlockCoverageStatePartly() {
        assertThat(jacocoConverter.getBlockCoverageState("1", "1"), `is`(PARTLY_COVERED_KEY))
    }

    @Test
    fun getLineCoverageStateCovered() {
        assertThat(jacocoConverter.getLineCoverageState("0"), `is`(COVERED_KEY))
    }

    @Test
    fun getLineCoverageStateUncovered() {
        assertThat(jacocoConverter.getLineCoverageState("1"), `is`(UNCOVERED_KEY))
    }

    @Test
    fun isSimpleStatementSimple() {
        assertThat(jacocoConverter.isSimpleStatement("0", "0"), `is`(true))
    }

    @Test
    fun isSimpleStatementNotSimpleWithMissedBranches() {
        assertThat(jacocoConverter.isSimpleStatement("1", "0"), `is`(false))
    }

    @Test
    fun isSimpleStatementNotSimpleWithCoveredBranches() {
        assertThat(jacocoConverter.isSimpleStatement("0", "1"), `is`(false))
    }

    @Test(expected = IllegalArgumentException::class)
    fun getCoverageThrowsExceptionForInvalidCounters() {
        val counters = listOf<Counter>()
        jacocoConverter.getCoverage(ANY_STRING, counters)
    }

    @Test
    fun getCoverage() {
        val coveredExpected = "1"
        val missedExpected = "2"

        val testCounter: Counter = Mockito.mock(Counter::class.java)
        `when`(testCounter.type).thenReturn(ANY_STRING)
        `when`(testCounter.covered).thenReturn(coveredExpected)
        `when`(testCounter.missed).thenReturn(missedExpected)

        val counters = mutableListOf<Counter>()
        counters.add(testCounter)
        val (covered, missed) = jacocoConverter.getCoverage(ANY_STRING, counters)

        assertThat(covered, `is`(coveredExpected))
        assertThat(missed, `is`(missedExpected))
    }

}
