package com.atlassian.bitbucket.coverage;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.PARTLY_COVERED_COLUMN;
import static com.atlassian.bitbucket.coverage.dao.AoFileCoverage.UNCOVERED_COLUMN;
import static com.google.common.collect.Sets.newHashSet;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.StringUtils.join;

@Component
public class CoverageParser {
    private static final Map<String, String> COLUMNS_MAP = new HashMap<String, String>() {{
        put("C", COVERED_COLUMN);
        put("P", PARTLY_COVERED_COLUMN);
        put("U", UNCOVERED_COLUMN);
    }};
    private static final String COVERAGE_SEPARATOR = ";";
    private static final String COVERAGE_LINES_SEPARATOR = ",";
    private static final String COVERAGE_TYPE_SEPARATOR = ":";
    private static final Set<String> COVERAGE_TYPES = newHashSet("C", "P", "U");

    public Map<String, Set<String>> parseCoverage(String coverage) {
        Map<String, Set<String>> result = new HashMap<>();
        for (String coverageType: COLUMNS_MAP.values()) {
            result.put(coverageType, newHashSet());
        }
        for (String coverageType: coverage.split(COVERAGE_SEPARATOR)) {
            String[] coverageInfo = coverageType.split(COVERAGE_TYPE_SEPARATOR);
            if (coverageInfo.length == 2) {
                result.put(COLUMNS_MAP.get(coverageInfo[0]), coverageToLines(coverageInfo[1]));
            }
        }
        return result;
    }

    public Map<String, Set<String>> parseCoverage(String covered, String partlyCovered, String uncovered) {
        Map<String, Set<String>> result = new HashMap<>();
        result.put(COVERED_COLUMN, coverageToLines(covered));
        result.put(PARTLY_COVERED_COLUMN, coverageToLines(partlyCovered));
        result.put(UNCOVERED_COLUMN, coverageToLines(uncovered));
        return result;
    }

    public Set<String> coverageToLines(String coverage) {
        return isNotBlank(coverage) ? newHashSet(coverage.split(COVERAGE_LINES_SEPARATOR)) : newHashSet();
    }

    public String linesToCoverage(Set<String> lines) {
        return join(lines, COVERAGE_LINES_SEPARATOR);
    }

    protected boolean isValidCoverage(String coverageString) {
        return isNotBlank(coverageString) && hasDataInCoverage(coverageString);
    }

    protected boolean hasDataInCoverage(String coverageString) {
        String[] coverageEntries = coverageString.split(COVERAGE_SEPARATOR);
        boolean hasDataInCoverage = false;
        for (String coverageEntry: coverageEntries) {
            hasDataInCoverage |= hasDataInCoverageEntry(coverageEntry);
        }

        return hasDataInCoverage;
    }

    protected boolean hasDataInCoverageEntry(String coverageEntry) {
        String[] coverageInfo = coverageEntry.split(COVERAGE_TYPE_SEPARATOR);
        String coverageType = coverageInfo[0];
        if (!COVERAGE_TYPES.contains(coverageType)) {
            return false;
        }

        if (coverageInfo.length != 2) {
            return false;
        }

        String[] coverageEntryLines = coverageInfo[1].split(COVERAGE_LINES_SEPARATOR);

        return everyLineNumberIsValidInt(coverageEntryLines);
    }

    protected boolean everyLineNumberIsValidInt(String[] coverageEntryLines) {
        for (String coverageLine: coverageEntryLines) {
            try {
                Integer.valueOf(coverageLine);
            } catch (NumberFormatException nfe) {
                return false;
            }
        }
        return true;
    }

}
