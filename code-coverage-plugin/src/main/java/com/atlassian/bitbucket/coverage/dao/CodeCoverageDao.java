package com.atlassian.bitbucket.coverage.dao;

public interface CodeCoverageDao {

    AoFileCoverage addFileCoverage(String commitId, String path, String coverage);

    AoFileCoverage findFileCoverage(String commitId, String path);

    int deleteCoverageForCommit(String commitId);

}
