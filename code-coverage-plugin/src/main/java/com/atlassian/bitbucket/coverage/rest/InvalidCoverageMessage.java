package com.atlassian.bitbucket.coverage.rest;

import com.atlassian.bitbucket.coverage.InvalidCoverageException;
import com.atlassian.bitbucket.rest.RestErrorMessage;

public class InvalidCoverageMessage extends RestErrorMessage {

    public InvalidCoverageMessage(InvalidCoverageException e) {
        super(e.getMessage());

        put("invalidEntities", e.getInvalidEntities());
    }

}
