package com.atlassian.bitbucket.coverage;

import com.atlassian.bitbucket.codeinsights.report.GetInsightReportRequest;
import com.atlassian.bitbucket.codeinsights.report.InsightReport;
import com.atlassian.bitbucket.codeinsights.report.InsightReportService;
import com.atlassian.bitbucket.codeinsights.report.SetInsightReportRequest;
import com.atlassian.bitbucket.idx.CommitIndex;
import com.atlassian.bitbucket.idx.IndexedCommit;
import com.atlassian.bitbucket.repository.Repository;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static com.atlassian.bitbucket.coverage.InsightReportManager.PLUGIN;
import static com.atlassian.bitbucket.coverage.InsightReportManager.PROVIDER_KEY;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InsightReportManagerTest {

    private static final String COMMIT_ID = "0a433dc801941a4173fbb80128aaa9966b324a7d";

    @Mock
    private CommitIndex commitIndex;
    @Mock
    private IndexedCommit indexedCommit;
    @InjectMocks
    private InsightReportManager insightReportManager;
    @Mock
    private InsightReportService insightReportService;
    @Mock
    private Repository repo1;
    @Mock
    private Repository repo2;

    @Before
    public void setUp() {
        when(indexedCommit.getRepositories()).thenReturn(ImmutableSet.of(repo1));
        when(commitIndex.getCommit(COMMIT_ID)).thenReturn(indexedCommit);
    }

    @Test
    public void createInsightReport() {
        insightReportManager.createInsightReport(COMMIT_ID);

        assertGetRequest(repo1);
        assertSetRequest(repo1);
        verifyNoMoreInteractions(insightReportService);
    }

    @Test
    public void createInsightReportForSeveralRepositories() {
        when(indexedCommit.getRepositories()).thenReturn(ImmutableSet.of(repo1, repo2));

        insightReportManager.createInsightReport(COMMIT_ID);

        assertGetRequest(repo1, repo2);
        assertSetRequest(repo1, repo2);
        verifyZeroInteractions(insightReportService);
    }

    @Test
    public void createInsightReportWhenReportAlreadyExists() {
        InsightReport insightReport = mock(InsightReport.class);
        when(insightReportService.get(any(GetInsightReportRequest.class))).thenReturn(Optional.of(insightReport));

        insightReportManager.createInsightReport(COMMIT_ID);

        assertGetRequest(repo1);
        verifyNoMoreInteractions(insightReportService);
    }

    @Test
    public void createInsightReportWhenReportExistsInOneRepoButNotTheOther() {
        when(indexedCommit.getRepositories()).thenReturn(ImmutableSet.of(repo1, repo2));
        InsightReport insightReport = mock(InsightReport.class);
        when(insightReportService.get(any(GetInsightReportRequest.class)))
                .thenReturn(Optional.of(insightReport))
                .thenReturn(Optional.empty());

        insightReportManager.createInsightReport(COMMIT_ID);

        assertGetRequest(repo1, repo2);
        assertSetRequest(repo2);
        verifyNoMoreInteractions(insightReportService);
    }

    @Test
    public void createInsightReportWhenNoIndexedCommitFound() {
        when(commitIndex.getCommit(COMMIT_ID)).thenReturn(null);

        insightReportManager.createInsightReport(COMMIT_ID);

        verifyZeroInteractions(insightReportService);
    }

    @Test
    public void createInsightReportWhenNoIndexedRepositoriesFound() {
        when(indexedCommit.getRepositories()).thenReturn(ImmutableSet.of());

        insightReportManager.createInsightReport(COMMIT_ID);

        verifyZeroInteractions(insightReportService);
    }

    private void assertGetRequest(Repository... repos) {
        ArgumentCaptor<GetInsightReportRequest> requestCaptor = ArgumentCaptor.forClass(GetInsightReportRequest.class);
        verify(insightReportService, times(repos.length)).get(requestCaptor.capture());

        int i = 0;
        List<GetInsightReportRequest> requests = requestCaptor.getAllValues();
        for (GetInsightReportRequest request : requests) {
            assertThat(request.getCommitId(), equalTo(COMMIT_ID));
            assertThat(request.getRepository(), equalTo(repos[i++]));
            assertThat(request.getKey(), equalTo(PROVIDER_KEY));
        }
    }

    private void assertSetRequest(Repository... repos) {
        ArgumentCaptor<SetInsightReportRequest> requestCaptor = ArgumentCaptor.forClass(SetInsightReportRequest.class);
        verify(insightReportService, times(repos.length)).set(requestCaptor.capture());

        int i = 0;
        List<SetInsightReportRequest> requests = requestCaptor.getAllValues();
        for (SetInsightReportRequest request : requests) {
            assertThat(request.getCommitId(), equalTo(COMMIT_ID));
            assertThat(request.getCoverageProviderKey(), equalTo(PLUGIN + ":" + PROVIDER_KEY));
            assertThat(request.getRepository(), equalTo(repos[i++]));
        }
    }
}
